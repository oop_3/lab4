import java.util.Scanner;

public class Array6 {
    public static void main(String[] args){
        int arr[] = new int[3];
        int sum = 0;
        
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.print("Plase input arr[" + i + "] :");
            arr[i] = sc.nextInt();
            sum = sum + arr[i];
        }
        double avg = (double)(sum)/3;
        System.out.print("arr = ");
        for (int i = 2; i >= 0; i--) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println("sum = "+sum);
        System.out.println("avg = "+avg);
    }
    
}
