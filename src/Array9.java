import java.util.Scanner;

public class Array9{
    public static void main(String[] args) throws Exception{
        int arr[] = new int[3];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.print("Plase input arr[" + i + "] :");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0; i < 3; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.print("please input search value : ");
        int find = sc.nextInt(), index = 0;
        boolean found = false;
        for (int i: arr) {
            if (i ==find) {
                found = true; break; 
            }
            index++;
        }
        System.out.println((found)? "found at index: "+ index : "index not found");
    }
}