import java.util.Scanner;

public class MyApp {
    public static void main(String[] args) throws Exception {
        while (true){
            WelcomeToMyApp();
            PrintManu();
            InputYourCohice();
        }
        

    }

    private static void PrintManu() {
        System.out.println("-- Manu --");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }

    static void InputYourCohice() {
        System.out.print("Plase input your choice : ");
        Scanner sc = new Scanner(System.in);
        int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    HelloNTime();
                    break;
                case 2:
                    Add2number();
                    break;
                case 3:
                    ExitProgram();
                    System.exit(0);
                default:
                    System.out.println("Error: Please input between 1-3 ");

            }
        

    }

    static void ExitProgram() {
        System.out.println("Bye !!!");
    }

    static void Add2number() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number: ");
        int first = sc.nextInt();
        System.out.print("Please input secound number: ");
        int second = sc.nextInt();
        System.out.println("Result = " + (first + second));
    }

    static void HelloNTime() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input time: ");
        int time = sc.nextInt();
        for (int i = 0; i < time; i++) {
            System.out.println("Hello World");
        }

    }

    static void WelcomeToMyApp() {
        System.out.println("Welcome to my app!!!");
    }

}
